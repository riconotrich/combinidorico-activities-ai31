
	function showGraph1(){

	var target = document.getElementById("graphCanvas1");
	var chart1 = new Chart(target, {

		type: 'bar',
			data: {
				labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul"],
				datasets: [
					{ 

					  label: 'Borrowed',
					  data: [150, 130, 70, 500, 350, 400, 360],
					  backgroundColor: "rgb(233,31,79)",

					},
					{ 

					  label: 'Returned',
					  data: [120, 90, 80, 300, 350, 400, 350],
					  backgroundColor: "rgb(239,107,33)",

					}

				]

			},

			options: {
				scales: {

					xAxes: [{

						barPercentage: 0.7,
			
						gridLines: {

							display: false
						},

						ticks: {

							fontSize: 10
						}

					}],
					yAxes: [{
						ticks: {

							suggestedMin: 0,
							suggestedMax: 600,
							fontSize: 10
						}

						
					}]

				},

				legend: {

                    position: "bottom",

                    labels: {

                    	boxWidth: 10,
                    	fontSize: 10

                    }

                }

			}

	});

	}

	function showGraph2(){

	var target = document.getElementById("graphCanvas2");
	var chart1 = new Chart(target, {

		type: 'doughnut',
			data: {
				labels: ["Romance", "Horror", "Mar", "Apr", "May"],
				datasets: [
					{ 

					  data: [45, 30, 3, 12, 10],
					  backgroundColor: [

					  	"rgb(233,31,79)",
					  	"rgb(239,107,33)",
					  	"rgb(21,117,233)",
					  	"rgb(8,188,145)",
					  	"rgb(255,192,0)"

					  ]

					},

				]

			},

			options: {

				cutoutPercentage: 60,

				scales: {

					xAxes: [{

						barPercentage: 0.7,
			
						gridLines: {

							drawBorder: false,
							display: false
						},

						ticks: {

							display: false
							
						}

					}],
					yAxes: [{

						gridLines: {

							drawBorder: false,
							display: false
						},

						ticks: {

							display: false
						}

						
					}]

				},

				legend: {

                    position: "bottom",

                    labels: {

                    	boxWidth: 10,
                    	fontSize: 10

                    }

                }

			}

	});

	}