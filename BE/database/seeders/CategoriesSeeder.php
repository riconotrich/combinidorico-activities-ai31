<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\Models\Categories;

use Carbon\Carbon;

class CategoriesSeeder extends Seeder
{
    public function run(){
       DB::table('categories')->insert(array(
       	array(
       	 'category' => 'Horror',
       	 'created_at' => Carbon::now(),
       	 'updated_at' => Carbon::now()
       	),
       	array(
       	 'category' => 'Romance',
       	 'created_at' => Carbon::now(),
       	 'updated_at' => Carbon::now()
       	),
       	array(
       	 'category' => 'Fantasy',
       	 'created_at' => Carbon::now(),
       	 'updated_at' => Carbon::now()
       	),
       	array(
       	 'category' => 'Sci-Fi',
       	 'created_at' => Carbon::now(),
       	 'updated_at' => Carbon::now()
       	),
       	array(
       	 'category' => 'Mystery',
       	 'created_at' => Carbon::now(),
       	 'updated_at' => Carbon::now()
       	),
       	array(
       	 'category' => 'Fiction',
       	 'created_at' => Carbon::now(),
       	 'updated_at' => Carbon::now()
       	),
       	array(
       	 'category' => 'Adventure',
       	 'created_at' => Carbon::now(),
       	 'updated_at' => Carbon::now()
       	)

       ));
    }
}
