<?php

namespace Database\Factories;

use App\Models\ReturnedBooks;
use Illuminate\Database\Eloquent\Factories\Factory;

class ReturnedBooksFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ReturnedBooks::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'patron_id' => 1,
            'copies' => $this->faker->randomDigit,
            'book_id' => 1
        ];
    }
}
