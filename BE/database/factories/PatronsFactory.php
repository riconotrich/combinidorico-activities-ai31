<?php

namespace Database\Factories;

use App\Models\Patrons;
use Illuminate\Database\Eloquent\Factories\Factory;

class PatronsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Patrons::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id' => 1,
            'last_name' => $this->faker->firstName,
            'first_name' => $this->faker->lastName,
            'middle_name' => $this->faker->lastName,
            'email' => $this->faker->safeEmail
        ];
    }
}
