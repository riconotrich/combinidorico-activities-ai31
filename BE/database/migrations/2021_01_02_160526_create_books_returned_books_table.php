<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksReturnedBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books_returned_books', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('books_id')->unsigned()->nullable();
            $table->foreign('books_id')
                  ->references('id')->on('books')
                  ->onDelete('cascade');
            $table->integer('returned_books_id')->unsigned()->nullable();
            $table->foreign('returned_books_id')
                  ->references('id')->on('returned_books')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books_returned_books');
    }
}
