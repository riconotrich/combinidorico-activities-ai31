<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBorrowedBooksPatronsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('borrowed_books_patrons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('patrons_id')->unsigned()->nullable();
            $table->foreign('patrons_id')
                  ->references('id')->on('patrons')
                  ->onDelete('cascade');
            $table->integer('borrowed_books_id')->unsigned()->nullable();
            $table->foreign('borrowed_books_id')
                  ->references('id')->on('borrowed_books')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('borrowed_books_patrons');
    }
}
