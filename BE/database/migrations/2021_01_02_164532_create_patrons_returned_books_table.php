<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatronsReturnedBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        Schema::create('patrons_returned_books', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('patrons_id')->unsigned()->nullable();
            $table->foreign('patrons_id')
                  ->references('id')->on('patrons')
                  ->onDelete('cascade');
            $table->integer('returned_books_id')->unsigned()->nullable();
            $table->foreign('returned_books_id')
                  ->references('id')->on('returned_books')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patrons_returned_books');
    }
}
