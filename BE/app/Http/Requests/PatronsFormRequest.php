<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\Response;

class PatronsFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'last_name' => array('required', 'string', 'regex:/(^([a-zA-Z ]+$))/'),
            'first_name' => array('required', 'string', 'regex:/(^([a-zA-Z ]+$))/'),
            'middle_name' => array('required', 'string', 'regex:/(^([a-zA-Z ]+$))/'),
            'email' => array('required', 'email')
        ];
    }

    public function messages()
    {
        return [ 
            'last_name.required' => 'You cannot leave the last name field blank',
            'last_name.regex' => 'Invalid format: last name should be in letters only',
            'first_name.required' => 'You cannot leave the first name field blank',
            'first_name.regex' => 'Invalid format: first name should be in letters only',
            'middle_name.required' => 'You cannot leave the middle name field blank',
            'middle_name.regex' => 'Invalid format: middle name should be in letters only',
            'email_name.required' => 'You cannot leave the email field blank'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }

}
