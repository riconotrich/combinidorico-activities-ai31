<?php

namespace App\Http\Requests;

use App\Models\BorrowedBooks;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\Response;

class ReturnedBooksFormRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $patronId = $this->patron_id;

        $bookId = $this->book_id;

        $borrowedBook = BorrowedBooks::where([
            ['patron_id', '=', $patronId],
            ['book_id', '=', $bookId]
        ])->first();

        return [
            'patron_id' => array('required', 'numeric'),
            'copies' => array('required', 'numeric', 'min:0', 'max:'.$borrowedBook->copies),
            'book_id' => array('required', 'numeric')
        ];
    }

    public function messages()
    {
        return [ 
            'patron.required' => 'You must select a patron',
            'copies.required' => 'You cannot leave the copies field blank',
            'copies.numeric' => 'Invalid input: copies should be an integer',
            'copies.min' => 'Invalid input: copies should be more than 0',
            'copies.max' => 'Invalid input: copies should not exceed borrowed copies',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }

}
