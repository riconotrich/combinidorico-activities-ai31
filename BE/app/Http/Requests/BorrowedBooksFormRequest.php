<?php

namespace App\Http\Requests;

use App\Models\Books;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\Response;

class BorrowedBooksFormRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $bookId = $this->book_id;
        $book = Books::find($bookId);
        return [
            'patron_id' => array('required', 'numeric'),
            'copies' => array('required', 'numeric', 'min:1', 'max:'.$book->copies),
            'book_id' => array('required', 'numeric')
        ];
    }

    public function messages()
    {
        return [ 
            'patron.required' => 'You must select a patron',
            'copies.required' => 'You cannot leave the copies field blank',
            'copies.numeric' => 'Invalid input: copies should be an integer',
            'copies.min' => 'Invalid input: copies should be more than 0',
            'copies.max' => 'Invalid input: copies should not exceed available copies',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }

}
