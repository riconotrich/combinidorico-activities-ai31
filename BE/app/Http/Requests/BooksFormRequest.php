<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\Response;

class BooksFormRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => array('required', 'string', 'regex:/(^([a-zA-Z: ]+))/'),
            'author' => array('required', 'string', 'regex:/(^([a-zA-Z. ]+$))/'),
            'copies' => array('required', 'numeric', 'gt:0'),
            'category_id' => array('required', 'numeric', 'gt:0')
        ];
    }

    public function messages()
    {
        return [ 
            'name.required' => 'You cannot leave the book name field blank',
            'name.regex' => 'Invalid format: book name should be in letters and numbers only',
            'author.required' => 'You cannot leave the author name field blank',
            'author.regex' => 'Invalid format: author name should be in letters only',
            'copies.required' => 'You cannot leave the copies field blank',
            'copies.numeric' => 'Invalid input: copies should be an integer',
            'copies.gt' => 'Invalid input: copies should be more than 0',
            'category_id.required' => 'A book should have a category'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }

}
