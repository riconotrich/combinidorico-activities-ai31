<?php

namespace App\Http\Controllers;

use App\Models\Books;
use App\Models\Categories;
use App\Http\Requests\BooksFormRequest;
use Illuminate\Http\Request;

class BooksController extends Controller
{
    public function index()
    {
        $getBooks = Books::with('categories')->get();

        return response()->json([
            'data' => $getBooks

        ], 200);
    }

    public function store(BooksFormRequest $request)
    {
        $newBook = Books::create($request->validated());

        $newBook->categories()->attach($request->only(['category_id']));

        return response()->json([
            'message' => 'Successfully added a new book!',
            'data' => $newBook

        ], 201);

    }

    public function show($book)
    {
        $getBook = Books::findOrFail($book);

        return response()->json([
            'data' => $getBook

        ], 200);
    }

    public function update(BooksFormRequest $request, $book)
    {
        $updateBook = Books::find($book);
        $updateBook->update($request->all());
        $updateBook->categories()->sync($request->only(['category_id']));

        $getBook = Books::find($book);

        return response()->json([
            'message' => 'Successfully updated a book!',
            'data' => $getBook

        ], 201);
    }

    public function destroy($book)
    {
        $deleteBook = Books::destroy($book);

        if($deleteBook){
            return response()->json([
                'message' => 'Successfully deleted a book!',
                'data' => $deleteBook

            ], 200);

        }else{
            return response()->json([
                'message' => 'Error deleting book!',

            ], 404);

        }
    }

}
