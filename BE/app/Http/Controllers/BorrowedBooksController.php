<?php

namespace App\Http\Controllers;

use App\Models\Books;
use App\Models\BorrowedBooks;
use App\Http\Requests\BorrowedBooksFormRequest;
use Illuminate\Http\Request;

class BorrowedBooksController extends Controller
{
    public function borrowBook(BorrowedBooksFormRequest $request, $id)
    {
    	$borrowBook = BorrowedBooks::create($request->validated());
        $borrowBook->patrons()->attach($request->only(['patron_id']));
        $borrowBook->books()->attach($id);

        $this->updateCopies($id, $request['copies']);

        return response()->json([
            'message' => 'Book borrowed successfully!',
            'data' => $borrowBook

        ], 201);
    }

    public function updateCopies($bookId, $copies)
    {
		$updateCopies = Books::find($bookId);
		$updateCopies->decrement('copies', $copies);
		$updateCopies->save();
	}
}
