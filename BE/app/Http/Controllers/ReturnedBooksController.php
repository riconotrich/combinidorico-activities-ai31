<?php

namespace App\Http\Controllers;

use App\Models\ReturnedBooks;
use App\Models\Books;
use App\Models\BorrowedBooks;
use App\Http\Requests\ReturnedBooksFormRequest;
use Illuminate\Http\Request;

class ReturnedBooksController extends Controller
{
    public function returnBook(ReturnedBooksFormRequest $request, $id)
    {
    	$returnBook = ReturnedBooks::create($request->validated());
        $returnBook->patrons()->attach($request->only(['patron_id']));
        $returnBook->books()->attach($id); 

        $this->updateCopies($id, $request['copies']);

        $this->removeBorrowedBook($id, $request['patron_id']);

        return response()->json([
            'message' => 'Book returned successfully!',
            'data' => $returnBook

        ], 201);
    }

    public function updateCopies($bookId, $copies)
    {
		$updateCopies = Books::find($bookId);
		$updateCopies->increment('copies', $copies);
		$updateCopies->save();
	}

    public function removeBorrowedBook($bookId, $patronId)
    {
        $borrowedBook = BorrowedBooks::where([
            ['patron_id', '=', $patronId],
            ['book_id', '=', $bookId]
        ])->first();
        $borrowedBook->delete();
    }
}
