<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function index()
    {
    	$getCategories = Categories::all();

        return response()->json([
            'data' => $getCategories

        ], 200);
    }
}
