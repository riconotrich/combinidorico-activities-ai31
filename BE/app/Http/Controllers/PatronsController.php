<?php

namespace App\Http\Controllers;

use App\Models\Patrons;
use App\Http\Requests\PatronsFormRequest;
use Illuminate\Http\Request;

class PatronsController extends Controller
{
    public function index()
    {
        $getPatrons = Patrons::all();

        return response()->json([
            'data' => $getPatrons

        ], 200);
    }

    public function store(PatronsFormRequest $request)
    {
        $newPatron = Patrons::create($request->validated());

        return response()->json([
            'message' => 'Successfully added a new patron!',
            'data' => $newPatron

        ], 201);

    }

    public function show($patrons)
    {
        $getPatron = Patrons::findOrFail($patrons);

        return response()->json([
            'data' => $getPatron

        ], 200);
    }

    public function update(PatronsFormRequest $request, $patrons)
    {
        $updatePatron = Patrons::find($patrons);
        $updatePatron->update($request->all());

        $getPatron = Patrons::find($patrons);

        return response()->json([
            'message' => 'Successfully updated a patron!',
            'data' => $getPatron

        ], 201);
    }

    public function destroy($patrons)
    {
        $deletePatron = Patrons::destroy($patrons);

        if($deletePatron){
            return response()->json([
                'message' => 'Successfully deleted a patron!',
                'data' => $deletePatron

            ], 200);

        }else{
            return response()->json([
                'message' => 'Error deleting patron!',

            ], 404);
        }
    }
}
