<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patrons extends Model
{
	use HasFactory;

	protected $fillable = ['last_name','first_name','middle_name','email'];

    public function borrowed_books()
    {
    	return $this->belongsToMany(BorrowedBooks::class);
    }

    public function returned_books()
    {
    	return $this->belongsToMany(ReturnedBooks::class);
    }
}
 