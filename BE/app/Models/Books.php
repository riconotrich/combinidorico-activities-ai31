<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
	use HasFactory;

    protected $fillable = ['name','author','copies','category_id'];

	public function categories()
    {
    	return $this->belongsToMany(Categories::class);
    }

    public function borrowed_books()
    {
    	return $this->belongsToMany(BorrowedBooks::class);
    }

    public function returned_books()
    {
    	return $this->belongsToMany(ReturnedBooks::class);
    }
}
