<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReturnedBooks extends Model
{
	use HasFactory;

	protected $fillable = ['patron_id','copies','book_id'];
	
    public function patrons()
    {
        return $this->belongsToMany(Patrons::class);
    }

    public function books()
    {
    	return $this->belongsToMany(Books::class);
    }
}
