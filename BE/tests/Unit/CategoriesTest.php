<?php

namespace Tests\Unit;

use App\Models\Books;
use App\Models\Categories;

use Illuminate\Foundation\Testing\RefreshDatabase;

use Tests\TestCase;

class CategoriesTest extends TestCase
{
	Use RefreshDatabase;

    public function test_if_category_has_many_books()
    {
        $category = Categories::factory()->create();
        $book = Books::factory()->create();

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $category->books);
    }

    public function test_categories_index()
    {
    	$this->json('GET', 'api/categories', ['Accept' => 'application/json'])
            ->assertStatus(200);
    }
}
