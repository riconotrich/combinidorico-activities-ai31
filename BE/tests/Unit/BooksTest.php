<?php

namespace Tests\Unit;

use App\Models\Books;
use App\Models\Categories;
use App\Models\Patrons;
use App\Models\BorrowedBooks;
use App\Models\ReturnedBooks;
use App\Http\Requests\BooksFormRequest;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Schema;

use Tests\TestCase;

class BooksTest extends TestCase
{
	use RefreshDatabase;

    public function test_if_book_has_many_categories()
    {
        $category = Categories::factory()->create();
        $book = Books::factory()->create();

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $book->categories);
        $this->assertEquals($category->id, $book->category_id);
    }

    public function test_if_book_has_many_borrowed_books()
    {
        $category = Categories::factory()->create();
        $book = Books::factory()->create();
        $patron = Patrons::factory()->create();
        $borrowed = BorrowedBooks::factory()->create();

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $book->borrowed_books);
    }

    public function test_if_book_has_many_returned_books()
    {
        $category = Categories::factory()->create();
        $book = Books::factory()->create();
        $patron = Patrons::factory()->create();
        $returned = ReturnedBooks::factory()->create();

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $book->returned_books);
    }

    public function test_books_index()
    {
        $this->json('GET', 'api/books', ['Accept' => 'application/json'])
            ->assertStatus(200);

    }

    public function test_if_a_book_is_stored()
    {
        $testCategory = Categories::factory()->create();  
        $testStoreData = [
            'name' => "El Filibusterismo",
            'author' => "Jose Rizal",
            'copies' => 5,
            'category_id' => 1
        ];

        $this->json('POST', 'api/books', $testStoreData, ['Accept' => 'application/json'])
            ->assertStatus(201)
            ->assertJson([
                "message" => "Successfully added a new book!"
            ]);
    }

    public function test_if_a_book_is_updated()
    {
        $testCategory = Categories::factory()->create();    
        $testBook = Books::factory()->create();

        $testUpdateData = [
            'name' => "El Filibusterismo",
            'author' => "Jose Rizal",
            'copies' => 5,
            'category_id' => 1
        ];

        $this->json('PUT', 'api/books/'.$testBook->id, $testUpdateData, ['Accept' => 'application/json'])
            ->assertStatus(201)
            ->assertJson(["message" => "Successfully updated a book!"]);
    }

    public function test_if_a_book_is_deleted()
    {
        $testCategory = Categories::factory()->create(); 
        $testBook = Books::factory()->create();

        $this->json('DELETE', 'api/books/'.$testBook->id, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson(["message" => "Successfully deleted a book!"]);
    }

    public function test_if_a_book_can_be_borrowed()
    {
        $testPatron = Patrons::factory()->create(); 
        $testCategory = Categories::factory()->create();
        $testBook = Books::factory()->create();

        $borrow = [
            'patron_id' => 1,
            'copies' => 1,
            'book_id' => 1  
        ];

        $this->json('POST', 'api/books/'.$testBook->id.'/borrow', $borrow, ['Accept' => 'application/json'])
            ->assertStatus(201)
            ->assertJson(["message" => "Book borrowed successfully!"]);
    }

    public function test_if_a_book_can_be_returned()
    {
        $testPatron = Patrons::factory()->create(); 
        $testCategory = Categories::factory()->create();
        $testBook = Books::factory()->create();
        $testBorrowedBook = BorrowedBooks::factory()->create();

        $borrow = [
            'patron_id' => 1,
            'copies' => 1,
            'book_id' => 1
        ];

        $this->json('POST', 'api/books/'.$testBook->id.'/return', $borrow, ['Accept' => 'application/json'])
            ->assertStatus(201)
            ->assertJson(["message" => "Book returned successfully!"]);
    }
    
    //*Test form request validations

    public function setUp() :void
    {
        parent::setUp();
        $this->rules = (new BooksFormRequest())->rules();
        $this->validator = $this->app['validator'];
    }

    public function test_validation_of_books_fields()
    {
        //test if form request accepts valid input

        $this->assertTrue($this->validateField('name', 'Test Title'));
        $this->assertTrue($this->validateField('author', 'John Doe'));
        $this->assertTrue($this->validateField('copies', 1));
        $this->assertTrue($this->validateField('category_id', 1));

        //test if form request rejects invalid input

        $this->assertFalse($this->validateField('name', ''));
        $this->assertFalse($this->validateField('author', 'Jo4h Do3'));
        $this->assertFalse($this->validateField('copies', 'A'));
        $this->assertFalse($this->validateField('category_id', 'A'));
    }

    protected function getFieldValidator($field, $value)
    {
        return $this->validator->make(
            [$field => $value],
            [$field => $this->rules[$field]]
        );
    }

    protected function validateField($field, $value)
    {
        return $this->getFieldValidator($field, $value)->passes();
    }
}
