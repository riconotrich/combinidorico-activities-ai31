<?php

namespace Tests\Unit;

use App\Models\Books;
use App\Models\Categories;
use App\Models\Patrons;
use App\Models\ReturnedBooks;

use Illuminate\Foundation\Testing\RefreshDatabase;

use Tests\TestCase;

class ReturnedBooksTest extends TestCase
{
	Use RefreshDatabase;

    public function test_if_returned_book_has_many_books()
    {
        $category = Categories::factory()->create();
        $book = Books::factory()->create();
        $patron = Patrons::factory()->create();
        $returned = ReturnedBooks::factory()->create();

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $returned->books);
        $this->assertEquals($book->id, $returned->book_id);
    }

    public function test_if_returned_book_has_many_patrons()
    {
        $category = Categories::factory()->create();
        $book = Books::factory()->create();
        $patron = Patrons::factory()->create();
        $returned = ReturnedBooks::factory()->create();

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $returned->patrons);
        $this->assertEquals($patron->id, $returned->patron_id);
    }
}
