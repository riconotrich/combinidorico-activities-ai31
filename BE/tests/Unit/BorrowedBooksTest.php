<?php

namespace Tests\Unit;

use App\Models\Books;
use App\Models\Categories;
use App\Models\Patrons;
use App\Models\BorrowedBooks;

use Illuminate\Foundation\Testing\RefreshDatabase;

use Tests\TestCase;

class BorrowedBooksTest extends TestCase
{
	Use RefreshDatabase;

    public function test_if_borrowed_book_has_many_books()
    {
        $category = Categories::factory()->create();
        $book = Books::factory()->create();
        $patron = Patrons::factory()->create();
        $borrowed = BorrowedBooks::factory()->create();

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $borrowed->books);
        $this->assertEquals($book->id, $borrowed->book_id);
    }

    public function test_if_borrowed_book_has_many_patrons()
    {
        $category = Categories::factory()->create();
        $book = Books::factory()->create();
        $patron = Patrons::factory()->create();
        $borrowed = BorrowedBooks::factory()->create();

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $borrowed->patrons);
        $this->assertEquals($patron->id, $borrowed->patron_id);
    }
}
