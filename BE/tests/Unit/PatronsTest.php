<?php

namespace Tests\Unit;

use App\Models\Books;
use App\Models\Categories;
use App\Models\Patrons;
use App\Models\BorrowedBooks;
use App\Models\ReturnedBooks;
use App\Http\Requests\PatronsFormRequest;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Tests\TestCase;

class PatronsTest extends TestCase
{
	use RefreshDatabase;

    public function test_if_patron_has_many_borrowed_books()
    {
        $category = Categories::factory()->create();
        $book = Books::factory()->create();
        $patron = Patrons::factory()->create();
        $borrowed = BorrowedBooks::factory()->create();

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $patron->borrowed_books);
    }

    public function test_if_patron_has_many_returned_books()
    {
        $category = Categories::factory()->create();
        $book = Books::factory()->create();
        $patron = Patrons::factory()->create();
        $returned = ReturnedBooks::factory()->create();

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $patron->returned_books);
    }

    public function test_patrons_index()
    {
        $this->json('GET', 'api/patrons', ['Accept' => 'application/json'])
            ->assertStatus(200);
    }

    public function test_if_a_patron_is_stored()
    {
        $testStoreData = [
            "last_name" => "Doe",
            "first_name" => "John",
            "middle_name" => "Ronald",
            "email" => "johndoe@example.com"

        ];

        $this->json('POST', 'api/patrons', $testStoreData, ['Accept' => 'application/json'])
            ->assertStatus(201)
            ->assertJson([
                "message" => "Successfully added a new patron!"

            ]);
    }

    public function test_if_a_patron_is_updated()
    {
        $testPatron = Patrons::factory()->create();

        $testUpdateData = [
            'last_name' => "Doe",
            'first_name' => "John",
            'middle_name' => "Ronald",
            'email' => "johndoe@example.com"
        ];

        $this->json('PUT', 'api/patrons/'.$testPatron->id, $testUpdateData, ['Accept' => 'application/json'])
            ->assertStatus(201)
            ->assertJson(["message" => "Successfully updated a patron!"]);
    }

    public function test_if_a_patron_is_deleted()
    {
        $testPatron = Patrons::factory()->create();

        $this->json('DELETE', 'api/patrons/'.$testPatron->id, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson(["message" => "Successfully deleted a patron!"]);
    }

    //*Test form request validations

    public function setUp() :void
    {
        parent::setUp();
        $this->rules = (new PatronsFormRequest())->rules();
        $this->validator = $this->app['validator'];
    }

    public function test_validation_of_patrons_fields()
    {
        //test if form request accepts valid input

        $this->assertTrue($this->validateField('last_name', 'Doe'));
        $this->assertTrue($this->validateField('first_name', 'John'));
        $this->assertTrue($this->validateField('middle_name', 'Robert'));
        $this->assertTrue($this->validateField('email', 'sampleemail@example.com'));

        //test if form request rejects invalid input

        $this->assertFalse($this->validateField('last_name', 'Do1'));
        $this->assertFalse($this->validateField('first_name', 'Jo4h'));
        $this->assertFalse($this->validateField('middle_name', 'R0b3rt'));
        $this->assertFalse($this->validateField('email', 'sampleemail'));
    }

    protected function getFieldValidator($field, $value)
    {
        return $this->validator->make(
            [$field => $value],
            [$field => $this->rules[$field]]
        );
    }

    protected function validateField($field, $value)
    {
        return $this->getFieldValidator($field, $value)->passes();
    }
    
}
