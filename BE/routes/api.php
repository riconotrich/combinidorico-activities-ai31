<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\BorrowedBooksController;
use App\Http\Controllers\ReturnedBooksController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('patrons', 'App\Http\Controllers\PatronsController', [
	'only' => ['index','store','show','update','destroy']
]);

Route::resource('books', 'App\Http\Controllers\BooksController', [
	'only' => ['index','store','show','update','destroy']
]);

Route::prefix('/books/{id}')->group( function(){
	Route::post('/borrow', [BorrowedBooksController::class, 'borrowBook']);
	Route::post('/return', [ReturnedBooksController::class, 'returnBook']);
	}	
);

Route::get('categories', 'App\Http\Controllers\CategoriesController@index');