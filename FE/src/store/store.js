import Vue from 'vue';
import Vuex from 'vuex'
import patronsModule from './modules/patrons'
import booksModule from './modules/books'
import categoriesModule from './modules/categories'

Vue.use(Vuex);

export const store = new Vuex.Store({
	modules: {
		patrons: patronsModule,
		books: booksModule,
		categories: categoriesModule
	}

})
