const allPatrons = state => state.patrons 
const showPatron = state => state.patron 

export default {
	allPatrons,
	showPatron
}