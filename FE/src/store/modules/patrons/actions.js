import axios from 'axios';

import {API_BASE_URL} from '../../../config';

	const fetchPatrons = ({commit}) =>{
		axios.get(API_BASE_URL + '/patrons')
		.then(response => {
			commit('FETCH_PATRONS', response.data.data)
		}).catch((error) => {
			console.log(error)
		})
	}

	const showPatron = ({commit}, id) =>{
		axios.get(API_BASE_URL + '/patrons/' + id)
		.then(response => {
			commit('SHOW_PATRON', response.data.data)
		}).catch((error) => {
			console.log(error)
		})
	}

	const addPatron = ({commit}, add) => {
		return new Promise((resolve, reject) => {
			axios.post(API_BASE_URL + '/patrons', add)
				.then((response) => {
					commit('ADD_PATRON', response.data)
					resolve(response);
				}).catch((error) => {
					reject(error);
				})
		})
	}

	const updatePatron = ({commit}, {edit, id}) => {
		return new Promise((resolve, reject) => {
			axios.put(API_BASE_URL + '/patrons/' + id, edit)
				.then((response) => {
					commit('UPDATE_PATRON', response.data)
					resolve(response);
				}).catch((error) => {
					reject(error);
				})
		})
	}

	const deletePatron = ({commit}, id) => {
		return new Promise((resolve, reject) => {
			axios.delete(API_BASE_URL + '/patrons/' + id)
				.then((response) => {
					commit('DELETE_PATRON', response.data)
					resolve(response);
				}).catch((error) => {
					reject(error);
				})
		})
	}

export default {
	fetchPatrons,
	showPatron,
	addPatron,
	updatePatron,
	deletePatron
}