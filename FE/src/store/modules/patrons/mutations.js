const FETCH_PATRONS = (state, patrons) => {
	state.patrons = patrons
}

const SHOW_PATRON = (state, patron) => {
	state.patron = patron
}

const ADD_PATRON = (state, patron) => {
	state.patrons.unshift(patron)
}

const UPDATE_PATRON = (state, updatedPatron) => {
	let patron = [...state.patrons];
	const index = patron.findIndex(p => p.id === updatedPatron.id);
	if(index !== -1){
		patron.splice(index, 1, updatedPatron);
	}

	state.patrons = [...patron];
}

const DELETE_PATRON = (state, patron) => state.patrons = state.patrons.filter(p => p.id !== patron.id)

export default{
	FETCH_PATRONS,
	SHOW_PATRON,
	ADD_PATRON,
	UPDATE_PATRON,
	DELETE_PATRON
} 