import axios from 'axios';

import {API_BASE_URL} from '../../../config';

	const fetchCategories = ({commit}) =>{
		axios.get(API_BASE_URL + '/categories')
		.then(response => {
			commit('FETCH_CATEGORIES', response.data.data)
		}).catch((error) => {
			console.log(error)
		})
	}

	

export default {
	fetchCategories
}