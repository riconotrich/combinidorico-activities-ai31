const allBooks = state => state.books 
const showBook = state => state.book 

export default {
	allBooks,
	showBook
}