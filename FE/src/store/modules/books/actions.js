import axios from 'axios';

import {API_BASE_URL} from '../../../config';

	// Books CRUD function

	const fetchBooks = ({commit}) =>{
		axios.get(API_BASE_URL + '/books')
		.then(response => {
			commit('FETCH_BOOKS', response.data.data)
		}).catch((error) => {
			console.log(error)
		})
	}

	const showBook = ({commit}, id) =>{
		axios.get(API_BASE_URL + '/books/' + id)
		.then(response => {
			commit('SHOW_BOOK', response.data.data)
		}).catch((error) => {
			console.log(error)
		})
	}

	const addBook = ({commit}, add) => {
		return new Promise((resolve, reject) => {
			axios.post(API_BASE_URL + '/books', add)
				.then((response) => {
					commit('ADD_BOOK', response.data)
					resolve(response);
				}).catch((error) => {
					reject(error);
				})
		})
	}

	const updateBook = ({commit}, {edit, id}) => {
		return new Promise((resolve, reject) => {
			axios.put(API_BASE_URL + '/books/' + id, edit)
				.then((response) => {
					commit('UPDATE_BOOK', response.data)
					resolve(response);
				}).catch((error) => {
					reject(error);
				})
		})
	}

	const deleteBook = ({commit}, id) => {
		return new Promise((resolve, reject) => {
			axios.delete(API_BASE_URL + '/books/' + id)
				.then((response) => {
					commit('DELETE_BOOK', response.data)
					resolve(response);
				}).catch((error) => {
					reject(error);
				})
		})
	}

	// Books borrow/return functions

	const borrowBook = ({commit}, {borrowed, id}) => {
		return new Promise((resolve, reject) => {
			axios.post(API_BASE_URL + '/books/' + id + '/borrow', borrowed)
				.then((response) => {
					commit('BORROW_BOOK', response.data)
					resolve(response);
				}).catch((error) => {
					reject(error);
				})
		})
	}

	const returnBook = ({commit}, {returned, id}) => {
		return new Promise((resolve, reject) => {
			axios.post(API_BASE_URL + '/books/' + id + '/return', returned)
				.then((response) => {
					commit('RETURN_BOOK', response.data)
					resolve(response);
				}).catch((error) => {
					reject(error);
				})
		})
	}

export default {
	fetchBooks,
	showBook,
	addBook,
	updateBook,
	deleteBook,
	borrowBook,
	returnBook
}