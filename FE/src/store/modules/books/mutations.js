const FETCH_BOOKS = (state, books) => {
	state.books = books
}

const SHOW_BOOK = (state, book) => {
	state.book = book
}

const ADD_BOOK = (state, book) => {
	state.books.unshift(book)
}

const UPDATE_BOOK = (state, updatedBook) => {
	let book = [...state.books];
	const index = book.findIndex(b => b.id === updatedBook.id);
	if(index !== -1){
		book.splice(index, 1, updatedBook);
	}

	state.books = [...book];
}

const DELETE_BOOK = (state, book) => state.books = state.books.filter(b => b.id !== book.id)

const BORROW_BOOK = (state, book) => {
	state.borrowedBooks.unshift(book)
}

const RETURN_BOOK = (state, book) => {
	state.returnedBooks.unshift(book)
}

export default{
	FETCH_BOOKS,
	SHOW_BOOK,
	ADD_BOOK,
	UPDATE_BOOK,
	DELETE_BOOK,
	BORROW_BOOK,
	RETURN_BOOK
} 