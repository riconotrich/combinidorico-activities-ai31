import actions from './actions'
import mutations from './mutations'
import getters from './getters'

const state = {
	books: [],
	book: [],
	borrowedBooks: [],
	returnedBooks: []
}

export default({
	namespaced: true,
    state,
    actions,
    getters,
    mutations

})