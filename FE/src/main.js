import Vue from 'vue'
import App from '@/views/App.vue'
import router from '@/router'

import Vuex from 'vuex'
import BootstrapVue from 'bootstrap-vue'
import Toast from 'vue-toastification'

import {store} from './store/store';

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'vue-toastification/dist/index.css'

Vue.use(BootstrapVue);
Vue.use(Toast);
Vue.use(Vuex);

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router,
  store: store

}).$mount('#app')
