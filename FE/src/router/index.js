import Vue from 'vue'
import Router from 'vue-router'

import Dashboard from '@/components/pages/Dashboard'
import Patron_Management from '@/components/pages/Patron_Management'
import Book_Management from '@/components/pages/Book_Management' 
import Settings from '@/components/pages/Settings'  

Vue.use(Router);

const routes = [

	{
		path: '/',
		name: 'Dashboard',
		component: Dashboard
	},

    {
        path: '/Patron_Management',
        name: 'Patron_Management',
        component: Patron_Management
    },

    {
        path: '/Book_Management',
        name: 'Book_Management',
        component: Book_Management
    },

    {
        path: '/Settings',
        name: 'Settings',
        component: Settings
    }

]

const router = new Router({
  routes
})

export default router